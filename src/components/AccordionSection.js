import React, { Component } from "react";
import PropTypes from "prop-types";

class AccordionSection extends Component {
  static propTypes = {
    children: PropTypes.instanceOf(Object).isRequired,
    isOpen: PropTypes.bool.isRequired,
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
  };

  onClick = () => {
    this.props.onClick(this.props.label);
  };

  render() {
    const { onClick, props: { isOpen, label } } = this;

    return (
      <div
        style={{
          background: isOpen ? "#373737;" : "",
          padding: isOpen ? "0% 0% 1% 0%":"0% 0% 1% 0%",
          color:"black"
        }}
      >
        <div onClick={onClick} style={{ cursor: "pointer",fontSize: "25px",fontWeight: "700",    borderTop: "1px solid #ebebeb",paddingTop: "1%" }}>
          {label}
          <div style={{ float: "left",marginRight: "10px"}}>
            {!isOpen && <span>&#43;</span>}
            {isOpen && <span>&#8722;</span>}
          </div>
        </div>
        {isOpen && (
          <div
            style={{
              marginTop: 10,
            }}
          >
            {this.props.children}
          </div>
        )}
      </div>
    );
  }
}

export default AccordionSection;
