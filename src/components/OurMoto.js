import React, { Component } from 'react'
// import { img } from 'react-bootstrap';
// import {Link} from 'react-router-dom'
import './css/ourmoto.css'

export default class OurMoto extends Component {
  render() {
    return (
      <div className="home-body">
        <div className="moto-1-div">
        </div>
        <div className="moto-2-div">
          <div className="moto-2-div-inner">
            <div className="moto-2-div-1">
              <div className="moto-2-div-1-align">
                <img src="assets/bul-300x300.png" className="moto-2-div-1-align moto-2-div-1-img" alt="Our Moto" />
              </div>
              <div className="moto-2-div-1-align mrg-tp">
                <h2>Making Your<br /> Ideas Possible</h2>
              </div>
              <div className="moto-2-div-1-align">
                <img src="assets/bul-300x300.png" className="moto-2-div-1-align moto-2-div-1-img" alt="Our Moto"/>
              </div>

            </div>
            <div className="moto-2-div-2 clearfix">
              <div className="moto-2-div-2-inner">
                22 Creatives passionately believe that your brand should reach to the right audience at the right time. As a best digital advertising agency,
                   we put our relentless work in all the possible fields. These fields include FMCG, Automobiles, Restaurants and much more. Digital Advertising
                    is very much crucial because whether you are on the threshold of entering into the market or you have an established organization, your positive
                     Brand’s presence is all what you want. A strong value system is provided by 22 Creatives to penetrate the public mind with trust and efficacious response.
                     <br /><br />
                Engaging people with your brand on all social media platforms like Facebook, Twitter, LinkedIn, Instagram is our Main motto. Promotions are achieved through
              Search Engine Optimization, Mobile Advertising, Pay Per Click, Email Marketing campaigns and other Digital Marketing Platforms.
            </div>

            </div>
            <div className="moto-2-div-3">
              <div className="moto-2-div-3-box border-ryt">
                <div className="moto-2-div-3-box-1">
                  <img src="assets/vector6.png" alt="Our Moto"/>
                </div>
                <div className="moto-2-div-3-box-2">
                  We Listen
              </div>
                <div className="moto-2-div-3-box-3">

                </div>

              </div>
              <div className="moto-2-div-3-box border-ryt">
                <div className="moto-2-div-3-box-1">
                  <img src="assets/stylich-icon.png" alt="Our Moto"/>
                </div>
                <div className="moto-2-div-3-box-2">
                  We Create
              </div>
                <div className="moto-2-div-3-box-3">

                </div>
              </div>
              <div className="moto-2-div-3-box">
                <div className="moto-2-div-3-box-1">
                  <img src="assets/parallax-icon.png" alt="Our Moto"/>
                </div>
                <div className="moto-2-div-3-box-2">
                  We Give Results
              </div>
                <div className="moto-2-div-3-box-3">

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
