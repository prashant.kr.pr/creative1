import React, { Component } from 'react'
// import { img } from 'react-bootstrap';
import './css/ourservice.css'
import { Link } from 'react-router-dom'

export default class OurServices extends Component {
  render() {
    return (
      <div>
        <div className="myservice-1-div">
          <div className="myservice-1-div-txt">
            <h1 className="myservice-1-div-txt-fsize">
              Testing.   Challanging.<br />
              Pushing Limits. Everyday
            </h1>
            <h2>
              Innovative Designing meets the Creative Zeal.
            </h2>
          </div>
        </div>

        <div className="moto-2-div">
          <div className="moto-2-div-inner">
            <div className="orservice">
              <div className="orservice-txt-hdr">
                <h1>
                  "OUR CREATIVE SERVICES "
              </h1>
              </div>
              <div className="orservice-2-div-2-inner">
                22 Creatives provide services which ensure to address all aspects of the consumer journey properly. So, by delivering real business outcomes
                 through effective application of digital media, 22 Creatives achieve uniformity. Most importantly, we excel at working ahead to optimize
                 conversion rate, traffic, leads. Moreover, when it comes to optimisation focusing on providing personalized results, We build spectacular
                 experiences. This is because we expand online visibility, creating a positive Brand Presence. We have a range of digital media solutions that
                  encompasses solutions at all levels. Your business can get  benefits with the services which our agency is offering. Some of the common benefits
                  include brand recognition, attracting potential customers, and more.
            </div>
              <div className="orservice-img">
                <div className="orservice-img-row1">
                  <div className="orservice-img-row1-1">
                    <a href="/OurServices">
                      <img alt="Our Services" src="assets/1-5-300x300.png" />
                    </a>
                  </div>
                  <div className="orservice-img-row1-2">
                    <a href="/OurServices">
                      <img alt="Our Services" src="assets/6-4-300x300.png" />
                    </a>
                  </div>
                  <div className="orservice-img-row1-3">
                    <a href="/OurServices">
                      <img alt="Our Services" src="assets/3-4-300x300.png" />
                    </a>
                  </div>
                </div>
                <div className="orservice-img-row2">
                  <div className="orservice-img-row1-1">
                    <a href="/OurServices">
                      <img alt="Our Services" src="assets/1-5-300x300.png" />
                    </a>
                  </div>
                  <div className="orservice-img-row1-2">
                    <a href="/OurServices">
                      <img alt="Our Services" src="assets/6-4-300x300.png" />
                    </a>
                  </div>
                  <div className="orservice-img-row1-3">
                    <a href="/OurServices">
                      <img alt="Our Services" src="assets/3-4-300x300.png" />
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="abt-buzz-btn" style={{ clear: "both" }}>
          <div className="abt-buzz-btn-inner">
            <Link to="/AboutUs" className="buzz-cls">BUZZ US</Link>
          </div>
        </div>
      </div>
    )
  }
}
