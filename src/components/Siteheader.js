import React, { Component } from 'react';
import './css/Siteheader.css'
import "bootstrap/dist/css/bootstrap.min.css"
import {Link} from 'react-router-dom'

export default class Siteheader extends Component {
    render() {
        return (
            <div className="site-header" >
                <div className="logo-img-cls">
                    <Link to="/">
                        <img src="assets/logo-22.png" className="logo-cls"  alt="22Creatives"/>
                    </Link>
                </div>
                <div className="header-menu">
                    <div className="menu-css">
                        <Link to="/OurMoto">
                            Our Moto
                        </Link>
                    </div>
                    <div className="menu-css">
                        <Link to="/AboutUs">
                            About US
                        </Link>
                    </div>
                    <div className="menu-css">
                        <Link to="/WorkPhilosophy">
                            Work Philosophy
                    </Link>
                    </div>
                    <div className="menu-css">
                        <Link to="/OurServices">
                            Our Services
                    </Link>
                    </div>
                    <div className="menu-css">
                        <Link to="/OurClients">
                            Our Clients
                    </Link>
                    </div>
                    <div className="menu-css">
                        <Link to="/ContactUs">
                            Contact Us
                    </Link>
                    </div>
                </div>
                <div className="clearboth">

                </div>
            </div>
        )
    }
}