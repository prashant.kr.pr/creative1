import React, { Component } from 'react'
import './css/AboutUs.css';
// import { img } from 'react-bootstrap';
import {Link} from 'react-router-dom'

export default class AboutUs extends Component {
    render() {
        return (
            <div>
                <div className="about-1-div">

                </div>
                <h1 className="abt-div1-heading txt-col-white">
                About us
                </h1>
                <p className="abt-div1-txt txt-col-white">
                    "We Combine your Awesome Ideas with our Atom of Unending Creations. That's how things Work"
                </p>
                <div className="abt-us-strgth">
                    <h3 className="abt-us-strgth-txt">
                        OUR STRENGTHS
                    </h3>
                    <div className="strgth-type">
                        <div className="strgth-type-inner">
                            <div className="strgth-col1">
                            <i class="far fa-thumbs-up hand-css"></i>
                                <div className="strgth-col1-inner-up abt-font">
                                    Faboulous Ferver
                            </div>
                            <i class="fas fa-cog hand-css"></i>
                                <div className="strgth-col1-inner-down abt-font">
                                    Collaboration
                            </div>
                            </div>
                            <div className="strgth-col2">
                            <i class="far fa-question-circle hand-css"></i>
                                <div className="strgth-col1-inner-up abt-font">
                                    Curiously Curious
                            </div>
                            <i class="fas fa-leaf hand-css"></i>
                                <div className="strgth-col1-inner-down  abt-font">
                                    Personal Touch
                            </div>
                            </div>
                            <div className="strgth-col3">
                            <i class="fas fa-check hand-css"></i>
                                <div className="strgth-col1-inner-up abt-font">
                                    Eminence in Excellence
                            </div>
                            <i class="far fa-clock hand-css"></i>
                                <div className="strgth-col1-inner-down abt-font">
                                    Result Driven
                            </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div className="abt-trgl">
                    <div className="trgl">
                        <img src="assets/about-us-trgl.png" alt="About US" />
                    </div>
                    <div className="trgl-txt">
                        <p>
                            We use our Wand of Digital Media Perception to Mold your Ideas<br /> with Stunning Appeal
                            </p>
                    </div>
                </div>
                <div className="abt-conct">
                    <div className="abt-conct-inner">
                        <div className="abt-conct-inner-box brder-rt">
                            <div className="abt-conct-img">
                                <img src="assets/contact-icon1.png"  alt="About US"/>
                            </div>
                            <div className="abt-conct-txt">
                                Our Creative Bombing Space
                            </div>
                            <div className="abt-conct-txt-p">
                                22 Creatives, 319 OCUS Quantam ,Sector-51, Gurgaon
                            </div>
                            <div className="abt-btm-line">
                            </div>
                        </div>
                        <div className="abt-conct-inner-box brder-rt">
                            <div className="abt-conct-img">
                                <img src="assets/contact-icon2.png" alt="About US"/>
                            </div>
                            <div className="abt-conct-txt">
                                Drop a line
                            </div>
                            <div className="abt-conct-txt-p">
                                getus@22creatives.com
                            </div>
                            <div className="abt-btm-line">
                            </div>
                        </div>
                        <div className="abt-conct-inner-box">
                            <div className="abt-conct-img">
                                <img src="assets/contact3.png" alt="About US"/>
                            </div>
                            <div className="abt-conct-txt">
                                Support
                            </div>
                            <div className="abt-conct-txt-p">
                                Phone: 0124 -7177198 , +91 - 9899899864
                            </div>
                            <div className="abt-btm-line">
                            </div>
                        </div>
                    </div>

                    <div className="abt-us-strngr">
                        <div className="abt-us-strngr1">
                            <h3 className="abt-us-strngr1-hdr">
                                HELLO SMART STRANGERS!!!
                            </h3>
                            <div className="abt-us-strngr1-sbhdr">
                                22 Creatives wishes you a convivial welcome
                                <i class="far fa-laugh-wink abt-us-smili"></i>
                            </div>
                            
                            <div className="abt-us-strngr1-p">
                                <p>
                                    22 CREATIVES is small group of seasoned and dedicated advertising men and women who believe
                                     in testing, challenging and pushing their limits every day. As a best digital Advertising
                                     Agency, we not only support a client’s business through digital media in rapidly changing markets but we continuously try to boost their Brand’s presence all the way.
                                </p>
                                <p>
                                    We strategise the Complete Online Digital Media that enables you to bring in value for your customer in today’s multi-brand culture. With advertising focused on the masses, we help you to establish a personalized relationship with the customer coupled with real time effort.
                                </p>
                                <p>
                                    Our cumulative experience in print has been evolving since the mid 90s and in digital media and digital marketing since its very beginning. Our work has graced categories ranging from FMCG, automobiles, education, home appliances, ministries, tourism, restaurants, and much more. While our unceasing quest for innovation has taken us into the arena of online publishing and information, we like nothing more than to achieve the next big milestone with you.
                                </p>
                            </div>

                        </div>
                        <div className="abt-us-strngr2">
                            <img src="assets/99.png" className="abt-us-strngr2-img"  alt="About US"/>
                        </div>
                    </div>
                </div>
                <div className="abt-buzz-btn">
                    <div className="abt-buzz-btn-inner">
                        <Link to="/AboutUs" className="buzz-cls">BUZZ US</Link>
                    </div>
                </div>
            </div>
        )
    }
}
