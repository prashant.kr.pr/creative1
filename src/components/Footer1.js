import React, { Component } from 'react'
import './css/Footer1.css';
// import { img} from 'react-bootstrap';

export default class Footer1 extends Component {
    
    render() {
        return (
            <footer className="footer">
                <span className="ftr-txt">
                    Let’s have a chat
                </span>
                <div className="min-wrapper clearfix">
                    <div className="item-wrapper trusts__itemWrapper">
                        <span className="extraBold--26 trusts__header w--arrow">
                            Build
                        </span>
                        <div className="ftr-txt-blow">
                            Help you build something
                        </div>
                    </div>
                    <div className="item-wrapper trusts__itemWrapper">
                        <span className="extraBold--26 trusts__header w--arrow">
                            Co-Incubate
                        </span>
                        <div className="ftr-txt-blow">
                            Co-Incubate Idea together
                        </div>
                    </div>
                    <div className="item-wrapper trusts__itemWrapper">
                        <span className="extraBold--26 trusts__header w--arrow">
                            Customise
                        </span>
                        <div className="ftr-txt-blow">
                            Help you build something
                        </div>
                    </div>
                    <div className="item-wrapper trusts__itemWrapper">
                        <span className="extraBold--26 trusts__header w--arrow">
                            Organise
                        </span>
                        <div className="ftr-txt-blow">
                            Help you build something
                        </div>
                    </div>
                </div>
                <div className="social__wrapper">
                    <a href="//www.facebook.com/" target="_blank" className="social__item">
                        <img alt="22Creatives" src="assets/fb-icon.svg" width="12" height="22" />
                    </a>
                    <a href="//www.facebook.com/" target="_blank" className="social__item">
                        <img alt="22Creatives" src="assets/fb-icon.svg" width="12" height="22" />
                    </a>
                    <a href="//www.facebook.com/" target="_blank" className="social__item">
                        <img alt="22Creatives" src="assets/fb-icon.svg" width="12" height="22" />
                    </a>
                    <a href="//www.facebook.com/" target="_blank" className="social__item">
                        <img alt="22Creatives" src="assets/fb-icon.svg" width="12" height="22" />
                    </a>
                </div>
                <div className="body--14 text--center">Copyright © 22Creatives. All Rights Reserved</div>
            </footer>
                )
            }
        }
