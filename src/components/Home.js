import React, { Component } from 'react';
// import { img } from 'react-bootstrap';
import './css/Home.css'

export default class Home extends Component {
    render() {
        return (
            <div className="home-body">
                <div className="home-1-div">

                </div>
                <div className="div1-txt">
                    Innovative Designing Meets The Creative Zeal
                </div>
                <div className="our-partners">
                    <p className="LandingClientTele__mainHeader">
                        Our proud partners
                    </p>
                    <div className="partner-collection">
                        <div className="clientTeleGrid__gridWrapper">
                            <img className="clientTeleGrid__gridItem" src="assets/7.png" alt="Pepsico" />
                            <img className="clientTeleGrid__gridItem" src="assets/2.png" alt="Devine" />
                            <img className="clientTeleGrid__gridItem" src="assets/3.png" alt="Semicolon" />
                            <img className="clientTeleGrid__gridItem" src="assets/4.png" alt="Ocus" />
                            <img className="clientTeleGrid__gridItem" src="assets/5.png" alt="Gokal" />
                            <img className="clientTeleGrid__gridItem" src="assets/6.png" alt="Biosash" />
                            <img className="clientTeleGrid__gridItem" src="assets/1.png" alt="Bridge Investment" />
                            <img className="clientTeleGrid__gridItem" src="assets/8.png" alt="The Paras World School" />
                        </div>
                    </div>

                    <div className="service-provide">
                        <p className="LandingClientTele__mainHeader">
                            Services We Provide
                        </p>
                        <div className="canDoCategory__gridWrapper">
                            <div className="canDoCategory__gridItemWrapper txt-col-white">
                                <div className="hom-ser-prvd-lft">
                                <h2>
                                    Website Development
                                </h2>
                                <h4>Development of the website</h4>
                                    <p className="hom-ser-prvd-p hom-p-txt">
                                    Buy whatever you need from any 7-Eleven branches islandwide and get rewarded for it. 
                                    Simply scan your personal 7Rewards barcode in the app to earn stamps.
                                     When you've collected enough stamps, make your selection from a range of goodies and redeem.
                                    </p>
                                </div>
                                <div className="hom-ser-prvd-ryt">
                                    <img alt="22Creatives" src="assets/heroImg-7rewards.png" />
                                </div>
                            </div>
                            <div className="canDoCategory__gridItemWrapper txt-col-white">
                            <div className="hom-ser-prvd-lft">
                                    <img alt="22Creatives" src="assets/heroImg-7rewards.png" />
                                </div>
                                <div className="hom-ser-prvd-ryt mrg-top">
                                <h2>
                                    Digital Marketing
                                </h2>
                                <h4>Development of the website</h4>
                                    <p className="hom-ser-prvd-p hom-p-txt">
                                    Buy whatever you need from any 7-Eleven branches islandwide and get rewarded for it. 
                                    Simply scan your personal 7Rewards barcode in the app to earn stamps.
                                     When you've collected enough stamps, make your selection from a range of goodies and redeem.
                                    </p>
                                </div>
                                
                            </div>
                            <div className="canDoCategory__gridItemWrapper txt-col-white">
                                <div className="hom-ser-prvd-lft">
                                <h2>
                                    Media Buying
                                </h2>
                                <h4>Development of the website</h4>
                                    <p className="hom-ser-prvd-p hom-p-txt">
                                    Buy whatever you need from any 7-Eleven branches islandwide and get rewarded for it. 
                                    Simply scan your personal 7Rewards barcode in the app to earn stamps.
                                     When you've collected enough stamps, make your selection from a range of goodies and redeem.
                                    </p>
                                </div>
                                <div className="hom-ser-prvd-ryt">
                                    <img alt="22Creatives" src="assets/heroImg-7rewards.png" />
                                </div>
                            </div>
                            <div className="canDoCategory__gridItemWrapper txt-col-white">
                            <div className="hom-ser-prvd-lft">
                                    <img alt="22Creatives" src="assets/heroImg-7rewards.png" />
                                </div>
                                <div className="hom-ser-prvd-ryt mrg-top">
                                <h2>
                                    Graphic Designing
                                </h2>
                                <h4>Development of the website</h4>
                                    <p className="hom-ser-prvd-p hom-p-txt">
                                    Buy whatever you need from any 7-Eleven branches islandwide and get rewarded for it. 
                                    Simply scan your personal 7Rewards barcode in the app to earn stamps.
                                     When you've collected enough stamps, make your selection from a range of goodies and redeem.
                                    </p>
                                </div>
                                
                            </div>
                            <div className="canDoCategory__gridItemWrapper txt-col-white">
                                <div className="hom-ser-prvd-lft">
                                <h2>
                                    Marketing Material
                                </h2>
                                <h4>Development of the website</h4>
                                    <p className="hom-ser-prvd-p hom-p-txt">
                                    Buy whatever you need from any 7-Eleven branches islandwide and get rewarded for it. 
                                    Simply scan your personal 7Rewards barcode in the app to earn stamps.
                                     When you've collected enough stamps, make your selection from a range of goodies and redeem.
                                    </p>
                                </div>
                                <div className="hom-ser-prvd-ryt">
                                    <img alt="22Creatives" src="assets/heroImg-7rewards.png" />
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div className="wrk-phylospy txt-col-white">
                <p class="wrk-phylospy-head">What we do</p>
                <div className="wrk-phy-sec">
                    <div className="wrk-phy-sec1">
                        <img  className="wrk-phy-sec1-img" src="assets/2JRVzkR.png"/>
                        <div className="wrk-phy-sec-hdr">
                            Art of
                        </div>
                        <div className="wrk-phy-sec-sbhdr">
                            In comparison to other Digital Advertising Agency, We provide you the flexibility so that you can choose 
                            from a range of presented services. We craft amazing digital work for all brands. Our Creative Areas 
                        </div>
                        
                    </div>
                    <div className="wrk-phy-sec1">
                        <img className="wrk-phy-sec1-img" src="assets/2JRVzkR.png"/>
                        <div className="wrk-phy-sec-hdr">
                            Branding
                        </div>
                        <div className="wrk-phy-sec-sbhdr">
                            In comparison to other Digital Advertising Agency, We provide you the flexibility so that you can choose 
                            from a range of presented services. We craft amazing digital work for all brands. Our Creative Areas 
                        </div>
                        
                    </div>
                    <div className="wrk-phy-sec1">
                        <img className="wrk-phy-sec1-img" src="assets/2JRVzkR.png"/>
                        <div className="wrk-phy-sec-hdr">
                            Creative Effect
                        </div>
                        <div className="wrk-phy-sec-sbhdr">
                        To further empower your ideas, we rely on our extensive experience and knowledge of industry trends to mold them with insight and stunning appeal.
                        </div>
                        
                    </div>
                </div>
                <div className="wrk-phy-second">
                    <div className="wrk-phy-sec1">
                        <img  className="wrk-phy-sec1-img" src="assets/2JRVzkR.png"/>
                        <div className="wrk-phy-sec-hdr">
                            Dedicated Support
                        </div>
                        <div className="wrk-phy-sec-sbhdr">
                        We don’t just make it a point to meet deadlines. Therefore, we offer clients with consultancy services to keep them in tune with latest Marketing
                         Strategy. Afterall, Coping up with changing scenarios, prevailing trends, beneficial insights is equally important.
                        </div>
                        
                    </div>
                    <div className="wrk-phy-sec1">
                        <img className="wrk-phy-sec1-img" src="assets/2JRVzkR.png"/>
                        <div className="wrk-phy-sec-hdr">
                            Engineering Idea
                        </div>
                        <div className="wrk-phy-sec-sbhdr">
                        An idea no matter how powerful needs to be honed, shaped and chiseled to reveal its full potential. Which is why we work hand in hand with our clients.
                        </div>
                        
                    </div>
                </div>
                </div>
            </div>
        )
    }
}