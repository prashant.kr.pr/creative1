import React, { Component } from 'react'
import './css/ourclients.css'
import {Link} from 'react-router-dom'
// import {img} from 'react-bootstrap'

export default class OurClients extends Component {
  render() {
    return (
      <div>
        <div className="our-client-1-div">

</div>
<div className="moto-2-div">
          <div className="ourclient-2-div-inner">
            <div className="orservice">
              <div className="orservice-txt-hdr">
                <h1>
                A list we are proud to present below
              </h1>
              </div>
              <div className="ourclient-2-div-2-inner">
              22 Creatives firmly hold the belief that service provider and clients are not two separate entities joined together out
               of necessity. But, they are a team working with the same agenda towards achieving the same goal of Creative Branding.
                We have not only worked in diverse segments but also honoured to have teamed-up with some of the most reputed and well
                 established names in the business. Our Clients are coming for Creative Branding. From Digital Marketing to Media buying,
                  printing and planning, we are proving our dedication all over Delhi-Ncr.
            </div>
              <div className="orservice-img pding-ryt">
                <div className="orservice-img-row1">
                  <div className="orservice-img-row1-1">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/1.png" className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-2">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/2.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-3">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/3.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-3">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/3.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                  </div>
                </div>
                <div className="orservice-img-row2">
                  <div className="orservice-img-row1-1">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/4.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-2">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/5.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-3">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/6.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-3">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/3.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                  </div>
                </div>
                <div className="orservice-img-row2">
                  <div className="orservice-img-row1-1">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/7.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-2">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/8.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-3">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/9.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-3">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/3.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                  </div>
                </div>
                <div className="orservice-img-row2">
                  <div className="orservice-img-row1-1">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/10.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-2">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/11.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-3">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/12.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-3">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/3.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                  </div>
                </div>
                <div className="orservice-img-row2">
                  <div className="orservice-img-row1-1">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/13.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-2">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/14.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-3">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/15.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-3">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/3.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                  </div>
                </div>
                <div className="orservice-img-row2">
                  <div className="orservice-img-row1-1">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/16.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-2">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/17.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-3">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/18.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-3">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/3.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                  </div>
                </div>
                <div className="orservice-img-row2">
                  <div className="orservice-img-row1-1">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/19.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-2">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/20.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-3">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/21.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-3">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/3.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                  </div>
                </div>
                <div className="orservice-img-row2">
                  <div className="orservice-img-row1-1">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/22.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-2">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/23.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-3">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/24.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-3">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/3.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                  </div>
                </div>
                <div className="orservice-img-row2">
                  <div className="orservice-img-row1-1">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/25.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-2">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/26.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-3">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/27.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-3">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/3.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                  </div>
                </div>
                <div className="orservice-img-row2">
                  <div className="orservice-img-row1-1">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/28.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-2">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/29.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-3">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/30.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-3">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/3.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                  </div>
                </div>
                <div className="orservice-img-row2">
                  <div className="orservice-img-row1-1">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/31.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-2">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/32.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-3">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/33.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-3">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/3.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                  </div>
                </div>
                <div className="orservice-img-row2">
                  <div className="orservice-img-row1-1">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/34.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-2">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/34.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                    <div class="orservice-ryt-line"></div>
                  </div>
                  <div className="orservice-img-row1-3">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/36.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                  </div>
                  <div className="orservice-img-row1-3">
                    <Link to="/OurServices">
                      <img alt="Our Clients" src="assets/clients/3.png"  className="orservice-img-dimns" />
                    </Link>
                    <div class="orservice-btm-line"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
