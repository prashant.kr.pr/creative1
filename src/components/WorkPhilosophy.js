import React, { Component } from 'react'
import './css/workphylosophy.css'
// import { img } from 'react-bootstrap'
import {Link} from 'react-router-dom'
import Accordion from './Accordion';

export default class WorkPhilosophy extends Component {
  render() {
    return (
      <div>
        <div className="wrk-phy-1-div">
        </div>
        <div className="wrk-phy-bdy">
          <div className="wrk-phy-bdy-inner">
            <div className="wrk-phy-bdy-inner-1">
              <div className="wrk-phy-bdy-inner-1-img">
                <img src="assets/client.png" alt="Work Philosophy"/>
              </div>
            </div>
            <Accordion allowMultipleOpen>
              <div label="What we do?" isOpen className="wrk-phy-bdy-inner-1-txt">
                <p>
                  We begin every creative process by thoroughly understanding a suitable media strategy for your organization,
                     segment, market audience and cater to your requirements as and when they emerge. What sets us apart is the
                      combination of indomitable team coupled with the best of technologies in the areas of digital marketing,
                       media strategies, printing and buying. Moreover, We also give the flexibility to select and choose from a range of presented services.
          </p>
              </div>
              <div label="We know A (Art of) and B (Branding) and combine it with CDE!!">
                <p>
                  In comparison to other Digital Advertising Agency, We provide you the flexibility so that you can choose from a
                   range of presented services. We craft amazing digital work for all brands. Our Creative Areas Cover Digital Marketing,
                    Graphic Designing, Media Buying & Planning, Media Printing, etc. Most importantly, Formulating an effective marketing
                    strategy is crucial for your business to establish a brand img and surpass the competition. Moreover, if you are uncertain
                     about any solution, we will guide you. We are the strategists and hence we help you in picking up the best of solutions or combinations if required.
          </p>

              </div>
              <div label="C (Creative Effect)">
                <p>
                  To further empower your ideas, we rely on our extensive experience and knowledge of industry trends to mold them
                   with insight and stunning appeal.
          </p>
              </div>
              <div label="D (Dedicated Support)">
                <p>
                  We don’t just make it a point to meet deadlines. Therefore, we offer clients with consultancy services to keep them
                   in tune with latest Marketing Strategy. Afterall, Coping up with changing scenarios, prevailing trends, beneficial insights is equally important.
          </p>
              </div>
              <div label="E (Engineering Ideas)">
                <p>
                  An idea no matter how powerful needs to be honed, shaped and chiseled to reveal its full potential. Which is why
                   we work hand in hand with our clients.
          </p>
              </div>
            </Accordion>
          </div>
          <div className="abt-buzz-btn">
                    <div className="abt-buzz-btn-inner">
                        <Link to="/AboutUs" className="buzz-cls">BUZZ US</Link>
                    </div>
                </div>
        </div>
      </div>
    )
  }
}
