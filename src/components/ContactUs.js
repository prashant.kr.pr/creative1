import React, { Component } from 'react';
import './css/contactus.css';
// import { img } from 'react-bootstrap'

export default class ContactUs extends Component {
  render() {
    return (
      <div>
        <div className="cntus">
          <div className="cntus-inner">
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14035.25635184122!2d77.068249!3d28.4248661!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3605ec59a45bf31f!2s22+Creatives!5e0!3m2!1sen!2sin!4v1555397189080!5m2!1sen!2sin"
              className="cntus-iframe"
              frameborder="0"
              title="22creative-gmap"
              style={{ border: "0" }} allowfullscreen></iframe>
            <div className="cntus-details">
              <div className="cntus-details-1">
                <img src="assets/hello.png" alt="Contact US"/>
              </div>
              <div className="cntus-details-2">
                <h2>
                  Let's have a chat!
                </h2>
                <div>
                  <h3>Phone</h3>
                  <span><a href="tel: 0124 - 7177198">0124 – 7177198</a></span>
                </div>
                <div>
                  <h3>Mob</h3>
                  <span><a href="tel:+91 - 9899899864">+91 – 9899899864</a></span>
                </div>
                <div>
                  <h3>Email</h3>
                  <span><a href="mailto:getus@22creatives.com">getus@22creatives.com</a></span>
                </div>
                <div>
                  <h3>Web</h3>
                  <span><a href="http://www.22creatives.com/" target="_blank" rel="noopener noreferrer">www.22creatives.com</a></span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
