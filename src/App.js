import React, { Component } from 'react';
import Siteheader from './components/Siteheader';
import Home from './components/Home';
import Footer1 from './components/Footer1';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import OurMoto from './components/OurMoto'
import AboutUs from './components/AboutUs'
import OurServices from './components/OurServices'
import WorkPhilosophy from './components/WorkPhilosophy'
import OurClients from './components/OurClients';
import ContactUs from './components/ContactUs'
// import Footer from './components/Footer';

class App extends Component {
  render() {
    return (
      <div>
        <Router>
          <Siteheader />
          {/* <Home 1/> */}
          <Route exact path="/" component={Home} />
          <Route path="/OurMoto" component={OurMoto} />
          <Route path="/AboutUs" component={AboutUs} />
          <Route path="/OurServices" component={OurServices} />
          <Route path="/WorkPhilosophy" component={WorkPhilosophy} />
          <Route path="/OurClients" component={OurClients} />
          <Route path="/ContactUs" component={ContactUs} />
        </Router>
        {/* <Footer /> */}
        <Footer1 />
      </div>
    );
  }
}

export default App;
